package org.bigtech.lambda;

import java.time.Duration;
import java.util.*;

/**
 * Java 8 lambda streams demo
 */
public class LambdaStreamsDemo {
    /**
     * Computes a given number is even or not
     *
     * @param number int
     * @return true/false
     */
    public static boolean isEven(final int number) {
        return number % 2 == 0;
    }

    static class MaxComparator implements Comparator<Integer>{
        @Override
        public int compare(final Integer o1, final Integer o2) {
            if(o1.intValue() > o2.intValue())
                return 1;
            else if(o1.intValue() < o2.intValue()){
                return -1;
            }
            else
                return 0;
        }
    }
    /**
     * main method
     *
     * @param args args
     */
    public static void main(final String[] args) {
        final List<Integer> elements = new ArrayList<>();
        Random random = new Random();
        for(long i = 0 ; i < 10000000; i++){
            elements.add(random.nextInt(500000));
        }
        // imperative code
        int maxValue = 0;
        long timeStartImp = System.nanoTime();
        for (final Integer number : elements) {
            if (maxValue < number) {
                maxValue = number;
            }
        }
        long timeEndImp = System.nanoTime();
        Duration timeSpentImp = Duration.ofNanos(timeEndImp - timeStartImp);
        System.out.println("Time Spent (Imp) : " + timeSpentImp.toMillis());
        System.out.println("Max Value: " + maxValue);
        // declarative code
        long timeStart = System.nanoTime();
        final int result = elements.stream().max(new MaxComparator()).get();
        long timeEnd = System.nanoTime();
        Duration timeSpent = Duration.ofNanos(timeEnd - timeStart);
        System.out.println("Max (declarative) : " + result);
        System.out.println("Time Spent : " + timeSpent.toMillis());
    }
}
