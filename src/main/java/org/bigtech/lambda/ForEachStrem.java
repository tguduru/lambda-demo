package org.bigtech.lambda;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Demonstrates forEach with lambda expressions. Under the hood java API does the same way as we have done so long in
 * imperative style of programming. But with lambda expressions the code will be more expressive and less noisy.
 * @author Thirupathi Reddy Guduru
 * @modified Dec 25, 2014
 */
public class ForEachStrem {

    /**
     * @param args
     */
    public static void main(final String[] args) {
        // this will create an immutable list
        final List<Integer> elements = Arrays.asList(1, 2, 3, 4, 5);
        // imperative
        System.out.println("Imperative");
        for (int i = 0; i < elements.size(); i++) {
            System.out.print(elements.get(i));
        }
        // fine grained
        System.out.println();
        for (final Integer val : elements) {
            System.out.print(val);
        }
        // Declarative
        System.out.println("\nDeclarative");
        // With Consumer interface
        elements.forEach(new Consumer<Integer>() {

            @Override
            public void accept(final Integer t) {
                System.out.print(t);
            }

        });
        System.out.println();
        // With lambda
        elements.forEach(val -> System.out.print(val));
        System.out.println();
        // fine grained - with method references
        elements.forEach(System.out::print);

        Stream<Integer> integerStream = elements.stream();
        integerStream.forEach(System.out::println);
        //streams can be consumed only once just like iterators as streams are internal iterations.
        //integerStream.forEach(System.out::println);
    }

}
