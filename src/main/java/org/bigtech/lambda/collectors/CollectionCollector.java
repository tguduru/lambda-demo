package org.bigtech.lambda.collectors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bigtech.lambda.Person;

/**
 * Demonstrating Collector example
 * @author Thirupathi Reddy Guduru
 * @modified Jan 5, 2015
 */
public class CollectionCollector {

    /**
     * @param args
     */
    public static void main(final String[] args) {
        final List<Person> persons = Arrays.asList(new Person("TOM", 20), new Person("BRENT", 25), new Person("KIRK",
                20), new Person("KENT", 30));

        // find out list persons whose age > 20 using stream filters,
        // it looks more imperative style than declarative as there are more moving parts and if the list doesn't
        // contain any person age > 20 then we still have an empty List
        final List<Person> olderThan20 = new ArrayList<>();
        persons.stream().filter(person -> person.getAge() > 20).forEach(olderThan20::add);
        System.out.println(olderThan20);

        // find out person older than using collectors. more declarative way
        final List<Person> olderThan20Persons = persons.stream().filter(person -> person.getAge() > 20)
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
        System.out.println(olderThan20Persons);

        // Simplified version of the above solution using Collectors utility methods.
        // Collectors utility class has many utility method for doing collection operations.
        final List<Person> olderThan20Person = persons.stream().filter(person -> person.getAge() > 20)
                .collect(Collectors.toList());
        System.out.println(olderThan20Person);

        // Some of utility methods of Collectors class.
        final Map<Integer, List<Person>> groupBy20Person = persons.stream().collect(
                Collectors.groupingBy(Person::getAge));
        System.out.println(groupBy20Person);

        // Another utility, only get the names of the persons group by age.
        final Map<Integer, List<String>> groupBy20PersonNames = persons.stream().collect(
                Collectors.groupingBy(Person::getAge, Collectors.mapping(Person::getName, Collectors.toList())));
        System.out.println(groupBy20PersonNames);
    }
}
