package org.bigtech.lambda.comparisons;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.bigtech.lambda.Person;

/**
 * Demonstrating {@link Comparator} with lambda streams.
 * @author Thirupathi Reddy Guduru
 * @modified Dec 27, 2014
 */
public class LambdaComparator {

    /**
     * @param args
     */
    public static void main(final String[] args) {
        final List<Person> persons = Arrays.asList(new Person("TOM", 20), new Person("BRENT", 25), new Person("KIRK",
                20));
        // Comparator , you can use a method which returns int value as a comparator
        persons.stream().sorted((person1, person2) -> person1.ageDifference(person2)).forEach(System.out::println);
        // reuse comparator
        final Comparator<Person> compareAscending = (person1, person2) -> person1.ageDifference(person2);
        final Comparator<Person> compareDescending = compareAscending.reversed();
        System.out.println("Age Ascending");
        persons.stream().sorted(compareAscending).forEach(System.out::println);
        System.out.println("Age Descending");
        persons.stream().sorted(compareDescending).forEach(System.out::println);

        // Find the lowest age person
        persons.stream().min(Person::ageDifference).ifPresent(youngest -> System.out.println("Youngest : " + youngest));

        // Find eldest
        persons.stream().max(Person::ageDifference).ifPresent(eldest -> System.out.println("Eldest : " + eldest));
    }
}
