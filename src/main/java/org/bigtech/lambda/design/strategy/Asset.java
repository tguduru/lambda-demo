package org.bigtech.lambda.design.strategy;

/**
 * A Simple POJO
 * @author Thirupathi Reddy Guduru
 * @modified Jan 13, 2015
 */
public class Asset {
    private final AssetType assetType;
    private final long value;

    /**
     * @param assetType
     * @param value
     */
    public Asset(final AssetType assetType, final long value) {
        super();
        this.assetType = assetType;
        this.value = value;
    }

    /**
     * @return the assetType.
     */
    public AssetType getAssetType() {
        return assetType;
    }

    /**
     * @return the value.
     */
    public long getValue() {
        return value;
    }

}
