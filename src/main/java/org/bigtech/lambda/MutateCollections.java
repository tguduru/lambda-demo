package org.bigtech.lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Demonstrates how we can mutate a collection using lambda expressions.
 * <p>
 * In this example we have given a {@link List} of {@link Integer}s and asked to get a new list with square of each
 * element of the given list.
 * <p>
 * @author Thirupathi Reddy Guduru
 * @modified Dec 25, 2014
 */
public class MutateCollections {
    public static Integer square(final Integer num) {
        return num * num;
    }

    /**
     * @param args
     */
    public static void main(final String[] args) {
        // this will create an immutable list
        final List<Integer> elements = Arrays.asList(1, 2, 3, 4, 5);
        // Imperative
        final List<Integer> newList = new ArrayList<Integer>();
        for (final Integer element : elements) {
            newList.add(square(element));
        }
        System.out.println("Finding square of each element of the following list");
        System.out.print(elements);
        System.out.println("\nImperative");
        System.out.print(newList);
        // Declarative
        System.out.println("\nDeclarative");
        // Some what functional , but still we need an empty collection
        final List<Integer> decList = new ArrayList<Integer>();
        elements.forEach(element -> decList.add(square(element)));
        System.out.print(decList);
        System.out.println();
        // lambda expression
        // with this it will avoid mutability as the value of element won't change, only it maps the a compute value of
        // element into its square, this will be useful when you don't want to mutate input.The map() creates a new List
        // then forEach() will iterate over it. This is pretty neat where lets say you want to add an element to a list
        // if a condition met otherwise you don't need to. In this scenario in imperative style we still create an empty
        // collection but with lambda expressions java won't create a list until the condition is met.
        elements.stream().map(element -> square(element)).forEach(element -> System.out.print(element + " "));
        System.out.println();
        // fine grained - with method references
        elements.stream().map(MutateCollections::square).forEach(System.out::print);
    }
}
