package org.bigtech.lambda;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Demonstrates reusing lambda expressions just like other objects.
 * @author Thirupathi Reddy Guduru
 * @modified Dec 25, 2014
 */
public class LambdaReuse {
    public static boolean isEven(final int number) {
        return number % 2 == 0;
    }

    public static Predicate<Integer> isEvenPredicate = number -> number % 2 == 0;

    // another form of reusable Predicate, this we can write a reusable predicate. This is called higher-order function,
    // a function which return another function.
    // NOTE: This is a static method, so all the variables will cached and might create issues, lets remove this by
    // using Functions
    public static Predicate<String> startsWith(final String letter) {
        return name -> name.startsWith(letter);
    }

    public static Function<String, Predicate<String>> startsWithFunction = (final String letter) -> {
        final Predicate<String> startsWithPredicate = name -> name.startsWith(letter);
        return startsWithPredicate;

    };

    /**
     * @param args
     */
    public static void main(final String[] args) {
        final List<Integer> listA = Arrays.asList(1, 2, 3, 4, 5);
        final List<Integer> listB = Arrays.asList(6, 7, 8, 9, 10);
        listA.stream().filter(isEvenPredicate).forEach(System.out::println);
        listB.stream().filter(isEvenPredicate).forEach(System.out::println);

        final List<String> cities = Arrays.asList("Greenville", "Columbus", "New York", "New Jersey", "Atlanta");
        // reusable Predicate
        System.out.println("Cities start with \"N\"");
        cities.stream().filter(startsWith("N")).forEach(System.out::println);
        System.out.println("Cities start with \"G\"");
        cities.stream().filter(startsWith("G")).forEach(System.out::println);
        // Functional Predicate
        System.out.println("Cities start with \"N\"");
        cities.stream().filter(startsWithFunction.apply("N")).forEach(System.out::println);
        System.out.println("Cities start with \"G\"");
        cities.stream().filter(startsWithFunction.apply("G")).forEach(System.out::println);

    }

}
