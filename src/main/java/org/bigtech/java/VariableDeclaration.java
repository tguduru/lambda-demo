package org.bigtech.java;

import java.util.ArrayList;
import java.util.List;

/**
 * Description.
 * @author Thirupathi Reddy Guduru
 * @modified Dec 14, 2014
 */
public class VariableDeclaration {

    /**
     * @param args
     */
    public static void main(final String[] args) {
        final List<Customer> stringObjects = new ArrayList<Customer>();
        Customer customer = null;
        for (int i = 0; i < 10000000; i++) {
            customer = new Customer("name", i, "city", "state", "country", "zip");
            stringObjects.add(customer);
        }
        while (true) {
            System.out.println("wait on it");
        }

    }

}
