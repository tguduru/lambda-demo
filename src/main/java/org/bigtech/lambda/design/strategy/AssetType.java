package org.bigtech.lambda.design.strategy;

/**
 * An enum for Asset Type
 * @author Thirupathi Reddy Guduru
 * @modified Jan 13, 2015
 */
public enum AssetType {
    BOND, STOCK;
}
