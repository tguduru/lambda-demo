package org.bigtech.lambda.functional;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author Guduru, Thirupathi Reddy
 */
public class ConsumerDemo {

    public static void main(String[] args) {
        List<String> strings = Arrays.asList("India","US","UK");
        Consumer<List<String>> stringConsumer = (List<String> s) -> s.forEach(System.out::println);
        stringConsumer.accept(strings);
    }
}
