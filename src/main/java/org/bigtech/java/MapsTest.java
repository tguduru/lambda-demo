package org.bigtech.java;

import java.util.HashMap;
import java.util.Map;

/**
 * @author hansi
 *
 * Created : Oct 15, 2014
 */
public class MapsTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EqualsTest a = new EqualsTest(2);
		EqualsTest b = new EqualsTest(2);
		Map map = new HashMap<EqualsTest,String>();
		map.put(a, "Thiru");
		map.put(b, "Hansi");
		EqualsTest c = a;
		map.put(c, "Santu");
		System.out.println(map.get(a));
		System.out.println(map.get(b));
	}

}
