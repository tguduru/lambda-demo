package org.bigtech.lambda;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Demonstrates the map-reduce pattern with lambda expressions.
 * @author Thirupathi Reddy Guduru
 * @modified Dec 26, 2014
 */
public class MapReduceDemo {
    public static void findHighestEven(final List<Integer> numbers) {
        final Optional<Integer> highest = numbers.stream().map(number -> number % 2 == 0 ? number : 0)
                .reduce((num1, num2) -> (num1 > num2) ? num1 : num2);
        System.out.println("Found highest even : " + highest.orElse(-1));
    }

    /**
     * @param args
     */
    public static void main(final String[] args) {
        final List<Integer> elements = Arrays.asList(1, 4, 3, 2, 6, 8, 4);
        System.out.println(elements);
        findHighestEven(elements);
    }

}
