package org.bigtech.lambda.comparisons;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import org.bigtech.lambda.Person;

/**
 * Demonstration {@link Function} with {@link Comparator} using lambda expressions.
 * @author Thirupathi Reddy Guduru
 * @modified Jan 5, 2015
 */
public class FunctionComparision {

    /**
     * @param args
     */
    public static void main(final String[] args) {
        final List<Person> persons = Arrays.asList(new Person("TOM", 20), new Person("BRENT", 25), new Person("KIRK",
                20));
        final Function<Person, String> byName = person -> person.getName();
        final Function<Person, Integer> byAge = person -> person.getAge();

        persons.stream().sorted(Comparator.comparing(byName).thenComparing(byAge)).forEach(System.out::println);
    }
}
