package org.bigtech.lambda.functional;

/**
 * @author Guduru, Thirupathi Reddy
 */
@FunctionalInterface
public interface SingleMethodInterface {
    String processString(String s1,String s2);
}
