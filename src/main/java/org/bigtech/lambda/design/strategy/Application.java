package org.bigtech.lambda.design.strategy;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * Demonstrates <a href="http://en.wikipedia.org/wiki/Strategy_pattern">strategy pattern</a> using lambda expressions.
 * In this example it demonstrate strategy pattern in method level.
 * <p>
 * Problem - find out sum of assets based on given asset type using {@link AssetType}
 * @author Thirupathi Reddy Guduru
 * @modified Jan 13, 2015
 */
public class Application {

    /**
     * @param args
     */
    public static void main(final String[] args) {
        final List<Asset> assets = Arrays.asList(new Asset(AssetType.BOND, 1000), new Asset(AssetType.STOCK, 2000),
                new Asset(AssetType.BOND, 2000), new Asset(AssetType.STOCK, 3000));
        System.out.println("Total Assets: " + assetsSome(assets));
        System.out.println("Bond Assets: " + assetsSome(assets, asset -> asset.getAssetType() == AssetType.BOND));
        System.out.println("Stock Assets: " + assetsSome(assets, asset -> asset.getAssetType() == AssetType.STOCK));
    }

    /**
     * This just uses lamdba expressions to find out the sum
     * @param assets
     * @return
     */
    private static long assetsSome(final List<Asset> assets) {
        return assets.stream().mapToLong(Asset::getValue).sum();
    }

    /**
     * Returns the sum of assets based on given AssetType, This uses strategy pattern where you compute based on given
     * strategy.
     * @param assets
     * @return
     */
    private static long assetsSome(final List<Asset> assets, final Predicate<Asset> assetType) {
        return assets.stream().filter(assetType).mapToLong(Asset::getValue).sum();
    }

}
