package org.bigtech.lambda.functional;

import java.util.Optional;

/**
 * @author Guduru, Thirupathi (TG028792)
 */
public interface OptionalFunctionalInterface {
    Optional<String> getName(String s);
}
