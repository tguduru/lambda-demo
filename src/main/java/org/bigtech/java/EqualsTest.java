package org.bigtech.java;

/**
 * @author hansi
 *
 * Created : Oct 15, 2014
 */
public class EqualsTest {
int i;
	/**
 * @param i
 */
public EqualsTest(int i) {
	super();
	this.i = i;
}

//	/* (non-Javadoc)
//	 * @see java.lang.Object#hashCode()
//	 */
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + i;
//		return result;
//	}
//
//	/* (non-Javadoc)
//	 * @see java.lang.Object#equals(java.lang.Object)
//	 */
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		EqualsTest other = (EqualsTest) obj;
//		if (i != other.i)
//			return false;
//		return true;
//	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EqualsTest a = new EqualsTest(2);
		EqualsTest b = new EqualsTest(2);
		EqualsTest c = a;
		System.out.println(a == b);
		System.out.println(a.equals(b));
		System.out.println(c == a);
	}
	
	

}
