package org.bigtech.lambda;

import java.util.Arrays;
import java.util.List;

/**
 * @author Guduru, Thirupathi (TG028792)
 */
public class MapDemo {
    public static void main(final String[] args) {
        final Person person = new Person(null, 10);
        final Person person1 = new Person("Tim", 20);
        final Person person2 = new Person("Paul Walker", 35);
        final List<Person> persons = Arrays.asList(person, person2, person2, person1);
        // this throws NPE, i should find out a way to guard against it.
        // when you are doing this in imperative way you may had an if condition to through out null values so same as
        // here we can add a filter to through them out.
        persons.stream().map(Person::getName).filter(s -> s != null).map(String::length).sorted()
                .forEach(System.out::println);
    }
}
