package org.bigtech.java;

/**
 * Demonstrating a child thread will kill once its job done but parent is still doing its job.
 * @author Thirupathi Reddy Guduru
 * @modified Jan 13, 2015
 */
public class ChildThreadDemo {

    /**
     * @param args
     */
    public static void main(final String[] args) {
        final RunnableThread runnableThread = new RunnableThread();
        final Thread thread = new Thread(runnableThread, "ChildThread");
        thread.start();
        while (true) {
            System.out.println("Still doing");
        }
    }
}
