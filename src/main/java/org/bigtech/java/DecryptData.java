package org.bigtech.java;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

/**
 * @author Guduru, Thirupathi Reddy
 * @modified 4/26/16
 */
public class DecryptData {
    public static void main(String[] args) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        String data = "TJUeP4iy0jUVOcsD2rNH9MDVSHKf0HIb1sdDzSSDWdzsHs485icnMCcef4+y\\nHyp/4i1jsz6sip3FyKvKHYZ9qQ==\\n";
        String keyData = "I0y16SQ4IgcYfJHiKUMjyw==\\n";
        Key key = new SecretKeySpec(keyData.getBytes(),"AEW");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE,key);
        String decryptedData = String.valueOf(cipher.doFinal(data.getBytes()));
        System.out.println(decryptedData);

    }
}
