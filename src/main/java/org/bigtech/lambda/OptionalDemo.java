package org.bigtech.lambda;

import org.bigtech.lambda.functional.OptionalFunctionalInterface;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Demonstrates the use of {@link Optional}
 * @author Thirupathi Reddy Guduru
 * @modified Dec 26, 2014
 */
public class OptionalDemo {
    public static void findName(final List<String> names, final String letter) {
        final Optional<String> found = names.stream().filter(name -> name.startsWith(letter)).findFirst();
        System.out.println("A string starts with: " + letter + " is: " + found.orElse("not found"));
    }

    public static void isPresentName(final List<String> names, final String letter) {
        final Optional<String> found = names.stream().filter(name -> name.startsWith(letter)).findFirst();
        found.ifPresent(name -> System.out.println("Found a string starts with:" + letter + " is: "
                + name.toUpperCase()));
    }

    /**
     * @param args
     */
    public static void main(final String[] args) {
        final List<String> strings = Arrays.asList("Work", "Class", "City", "Game");
        findName(strings, "D");
        findName(strings, "C");
        isPresentName(strings, "G");
        Optional<String> stringOptional = ((OptionalFunctionalInterface) s -> {
         if(s == null) return Optional.empty();
            return Optional.of(s);
        }).getName("OP");
        System.out.println(stringOptional.orElse("Input is null"));
    }

}