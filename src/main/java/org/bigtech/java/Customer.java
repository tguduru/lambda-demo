package org.bigtech.java;

/**
 * Description.
 * @author Thirupathi Reddy Guduru
 * @modified Dec 14, 2014
 */
public class Customer {
    private String name;
    private long id;
    private String city;
    private String state;
    private String country;
    private String zip;

    /**
     * @param name
     * @param id
     * @param city
     * @param state
     * @param country
     * @param zip
     */
    public Customer(final String name, final long id, final String city, final String state, final String country,
            final String zip) {
        this.name = name;
        this.id = id;
        this.city = city;
        this.state = state;
        this.country = country;
        this.zip = zip;
    }

    /**
     * @return the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @return the id.
     */
    public long getId() {
        return id;
    }

    /**
     * @return the city.
     */
    public String getCity() {
        return city;
    }

    /**
     * @return the state.
     */
    public String getState() {
        return state;
    }

    /**
     * @return the country.
     */
    public String getCountry() {
        return country;
    }

    /**
     * @return the zip.
     */
    public String getZip() {
        return zip;
    }

    /**
     * @param name the name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @param id the id to set.
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * @param city the city to set.
     */
    public void setCity(final String city) {
        this.city = city;
    }

    /**
     * @param state the state to set.
     */
    public void setState(final String state) {
        this.state = state;
    }

    /**
     * @param country the country to set.
     */
    public void setCountry(final String country) {
        this.country = country;
    }

    /**
     * @param zip the zip to set.
     */
    public void setZip(final String zip) {
        this.zip = zip;
    }
}
