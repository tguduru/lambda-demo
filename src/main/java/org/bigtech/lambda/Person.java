package org.bigtech.lambda;

/**
 * A Simple pojo
 * @author Thirupathi Reddy Guduru
 * @modified Dec 27, 2014
 */
public class Person {
    private String name;
    private Integer age;

    /**
     * @param name
     * @param age
     */
    public Person(final String name, final Integer age) {
        this.name = name;
        this.age = age;
    }

    /**
     * @return the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @return the age.
     */
    public Integer getAge() {
        return age;
    }

    /**
     * @param name the name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @param age the age to set.
     */
    public void setAge(final Integer age) {
        this.age = age;
    }

    /**
     * @param other
     * @return
     */
    public Integer ageDifference(final Person other) {
        return age - other.getAge();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Person [name=" + name + ", age=" + age + "]";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((age == null) ? 0 : age.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (age == null) {
            if (other.age != null) {
                return false;
            }
        } else if (!age.equals(other.age)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

}
