package org.bigtech.lambda.functional;

import java.util.function.Predicate;

/**
 * @author Guduru, Thirupathi Reddy
 */
public class PredicateDemo {
    public static void main(String[] args) {
        String str = "Hello! world";
        Predicate<String> stringPredicate = (String s) -> s.isEmpty();
        System.out.println(stringPredicate.test(str));
    }
}
