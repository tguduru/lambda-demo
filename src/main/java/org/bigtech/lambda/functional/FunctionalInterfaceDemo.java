package org.bigtech.lambda.functional;

/**
 * @author Guduru, Thirupathi Reddy
 *
 */
public class FunctionalInterfaceDemo {
    public static void main(String[] args) {
        String str = "hello";
        //Since SingleMethodInterface is a functional interface we can simply pass the code as an implementation of it.
        System.out.println(((SingleMethodInterface) (s1, s2) -> new StringBuilder().append(s1).append(s2).toString()).processString(str, str));
    }
}
