package org.bigtech.lambda.functional;

import java.util.function.Function;

/**
 * Demonstrates {@link java.util.function.Function}
 * @author Guduru, Thirupathi Reddy
 */
public class FunctionDemo {
    public static void main(String[] args) {
        String s = "Hello! World";
        Function<String,Integer> function = (String str) -> str.length();
        System.out.println(function.apply(s));
    }
}
